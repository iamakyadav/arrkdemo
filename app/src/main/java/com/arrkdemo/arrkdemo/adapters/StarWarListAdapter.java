package com.arrkdemo.arrkdemo.adapters;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.arrkdemo.arrkdemo.R;
import com.arrkdemo.arrkdemo.databinding.StarWarListRowBinding;
import com.arrkdemo.arrkdemo.interfaces.ClickInterface;
import com.arrkdemo.arrkdemo.models.StarWar;

import java.util.List;


public class StarWarListAdapter extends RecyclerView.Adapter<StarWarListAdapter.MyViewHolder> {

    private List<StarWar> starWarsList;
    private LayoutInflater layoutInflater;
    private Context context;
    private ClickInterface clickInterface;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final StarWarListRowBinding binding;

        public MyViewHolder(StarWarListRowBinding itemBinding) {
            super(itemBinding.getRoot());
            this.binding = itemBinding;
        }
    }

    public StarWarListAdapter(List<StarWar> starWarsList, ClickInterface clickInterface) {
        this.starWarsList = starWarsList;
        this.clickInterface = clickInterface;
    }

    public void setStarWarsList(List<StarWar> starWarsList) {
        this.starWarsList = starWarsList;
        this.notifyDataSetChanged();
    }

    @Override
    public StarWarListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.getContext());
        }
        context = parent.getContext();

        StarWarListRowBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.star_war_list_row, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(StarWarListAdapter.MyViewHolder holder, final int position) {
        holder.binding.setCharacter(starWarsList.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickInterface.setCharacter(starWarsList.get(position));

            }
        });
    }

    @Override
    public int getItemCount() {
        return starWarsList.size();
    }
}
