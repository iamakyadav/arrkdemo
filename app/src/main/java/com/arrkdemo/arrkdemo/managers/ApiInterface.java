package com.arrkdemo.arrkdemo.managers;


import com.arrkdemo.arrkdemo.models.StarWarList;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ApiInterface {

    @GET("people/")
    Call<StarWarList> getStarWarList(@Query("page") String apiKey);

}

