package com.arrkdemo.arrkdemo;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.arrkdemo.arrkdemo.databinding.ActivityMainBinding;
import com.arrkdemo.arrkdemo.fragments.CharacterDetailsFragment;
import com.arrkdemo.arrkdemo.fragments.CharacterListFragment;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        addFragment(new CharacterListFragment());
    }


    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout_main, fragment, "CharacterListFragment").addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }


    @Override
    public void onBackPressed() {
        List<Fragment> fragmentList = getSupportFragmentManager().getFragments();
        boolean isPopUp = false;
        for(Fragment fragment: fragmentList){
            if(fragment != null && fragment instanceof CharacterDetailsFragment){
                isPopUp = true;
            }
        }
        if(isPopUp){
            getSupportFragmentManager().popBackStack();
        }else {
            finish();
        }
    }
}
