package com.arrkdemo.arrkdemo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ApplicationUtils {

    private static int TYPE_NOT_CONNECTED                    = -1;
    public static String PARCELABLE_KEY                        = "starWar";

    public static boolean isNetworkConnected(Context context) {
        int connectivityType = getConnectivityType(context);
        if (connectivityType == TYPE_NOT_CONNECTED)
            return false;
        else
            return true;
    }

    public static int getConnectivityType(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (null != networkInfo && networkInfo.isConnected()) {
            return networkInfo.getType();
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String formatDate(String strDate) {
        String finalDateString = "";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date convertedDate = new Date();
        try {
            convertedDate = dateFormat.parse(strDate);
            SimpleDateFormat sdfnewformat = new SimpleDateFormat("dd MMM yyyy");
            finalDateString = sdfnewformat.format(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return finalDateString;
    }
}
