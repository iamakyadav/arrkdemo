package com.arrkdemo.arrkdemo.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arrkdemo.arrkdemo.ApplicationUtils;
import com.arrkdemo.arrkdemo.R;
import com.arrkdemo.arrkdemo.adapters.StarWarListAdapter;
import com.arrkdemo.arrkdemo.databinding.FragmentCharacterListBinding;
import com.arrkdemo.arrkdemo.interfaces.ClickInterface;
import com.arrkdemo.arrkdemo.managers.ApiClient;
import com.arrkdemo.arrkdemo.managers.ApiInterface;
import com.arrkdemo.arrkdemo.models.StarWar;
import com.arrkdemo.arrkdemo.models.StarWarList;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

public class CharacterListFragment extends Fragment implements ClickInterface, View.OnClickListener {

    private FragmentCharacterListBinding binding;

    StarWarListAdapter recListAdapter;
    private List<StarWar> starWarsList;

    private Call<StarWarList> call_resource;
    private ApiInterface apiService;
    private StarWarList api_response;
    private String paginationNextCount = "1";
    private String paginationPreviousCount = "";

    public void CharacterListFragment() {

    }


    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_character_list, group, false);

        View rootview = binding.getRoot();

        initResource();

        return rootview;
    }

    private void initResource() {
        binding.toolbar.tvNext.setOnClickListener(this);
        binding.toolbar.tvPrevious.setOnClickListener(this);
        binding.toolbar.tvNext.setVisibility(View.VISIBLE);
        apiService = ApiClient.getClient().create(ApiInterface.class);
        api_response = new StarWarList();
        starWarsList = new ArrayList<>();
        recListAdapter = new StarWarListAdapter(starWarsList, this);
        binding.recyclerList.setItemAnimator(new DefaultItemAnimator());
        binding.recyclerList.setAdapter(recListAdapter);
        recListAdapter.notifyDataSetChanged();


        if (ApplicationUtils.isNetworkConnected(getActivity())) {
            setLayoutVisibility(true);
            getCharactersList(paginationNextCount);
        } else {
            setLayoutVisibility(false);
        }


        binding.btnTryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ApplicationUtils.isNetworkConnected(getActivity())) {
                    setLayoutVisibility(true);
                    getCharactersList(paginationNextCount);
                } else {
                    setLayoutVisibility(false);
                }
            }
        });
    }


    public void getCharactersList(String page) {
        starWarsList.clear();
        setLayoutVisibility(true);
        binding.shimmerViewContainer.setVisibility(View.VISIBLE);
        binding.shimmerViewContainer.startShimmerAnimation();

        call_resource = apiService.getStarWarList(page);

        call_resource.enqueue(new Callback<StarWarList>() {
            @Override
            public void onResponse(Call<StarWarList> call, retrofit2.Response<StarWarList> response) {

                if (response != null && response.code() == 200) {
                    api_response = response.body();
                    starWarsList = api_response.getResults();

                    if (!TextUtils.isEmpty(api_response.next)) {
                        String nextCount[] = api_response.next.split("=");
                        paginationNextCount = nextCount[1];
                    } else {
                        binding.toolbar.tvNext.setVisibility(View.INVISIBLE);
                    }
                    if (!TextUtils.isEmpty(api_response.previous)) {
                        binding.toolbar.tvPrevious.setVisibility(View.VISIBLE);
                        String previousCount[] = api_response.previous.split("=");
                        paginationPreviousCount = previousCount[1];
                    } else {
                        binding.toolbar.tvPrevious.setVisibility(View.INVISIBLE);

                    }

                    binding.shimmerViewContainer.stopShimmerAnimation();
                    binding.shimmerViewContainer.setVisibility(View.GONE);
                    if (starWarsList.size() > 0) {
                        recListAdapter.setStarWarsList(starWarsList);
                    } else {
                        setLayoutVisibility(false);
                    }
                } else {
                    setLayoutVisibility(false);
                }
            }


            @Override
            public void onFailure(Call<StarWarList> call, Throwable t) {
                Log.e("status", "onFailure: ");
                setLayoutVisibility(false);
            }
        });
    }


    private void setLayoutVisibility(boolean isVisible) {
        if (isVisible) {
            binding.contraintMain.setVisibility(View.VISIBLE);
            binding.contraintRetry.setVisibility(View.GONE);
        } else {
            binding.contraintMain.setVisibility(View.GONE);
            binding.contraintRetry.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setCharacter(StarWar starWar) {
        addFragment(new CharacterDetailsFragment(), starWar);
    }

    private void addFragment(Fragment fragment, StarWar starWar) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(ApplicationUtils.PARCELABLE_KEY, starWar);
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragment.setArguments(bundle);
        transaction.hide(getFragmentManager().findFragmentByTag("CharacterListFragment"));
        transaction.add(R.id.frameLayout_main, fragment, "CharacterDetailsFragment").addToBackStack(null);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        transaction.commitAllowingStateLoss();
    }

    @Override
    public void onClick(View v) {

        if (v == binding.toolbar.tvNext) {
            getCharactersList(paginationNextCount);

        } else if (v == binding.toolbar.tvPrevious) {
            getCharactersList(paginationPreviousCount);

        }

    }
}
