package com.arrkdemo.arrkdemo.fragments;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arrkdemo.arrkdemo.ApplicationUtils;
import com.arrkdemo.arrkdemo.R;
import com.arrkdemo.arrkdemo.databinding.FragmentCharacterDetailsBinding;
import com.arrkdemo.arrkdemo.interfaces.ClickInterface;
import com.arrkdemo.arrkdemo.models.StarWar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CharacterDetailsFragment extends Fragment {

    private FragmentCharacterDetailsBinding binding;


    public void CharacterDetailsFragment() {

    }

    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup group, Bundle bundle) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_character_details, group, false);

        View rootview = binding.getRoot();

        initResource();

        return rootview;
    }

    private void initResource() {
        Bundle bundle = getArguments();
        StarWar starWar = bundle.getParcelable(ApplicationUtils.PARCELABLE_KEY);
        starWar.setCreated(ApplicationUtils.formatDate(starWar.getCreated()));
        binding.setCharacter(starWar);
    }


}
