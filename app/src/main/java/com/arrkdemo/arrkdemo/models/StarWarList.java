package com.arrkdemo.arrkdemo.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class StarWarList implements Parcelable {

    public int count;
    public String next;
    public String previous;
    private List<StarWar> results;

    public StarWarList() {
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getNext() {
        return next;
    }

    public void setNext(String next) {
        this.next = next;
    }

    public String getPrevious() {
        return previous;
    }

    public void setPrevious(String previous) {
        this.previous = previous;
    }

    public List<StarWar> getResults() {
        return results;
    }

    public void setResults(List<StarWar> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.count);
        dest.writeString(this.next);
        dest.writeString(this.previous);
        dest.writeTypedList(this.results);
    }

    protected StarWarList(Parcel in) {
        this.count = in.readInt();
        this.next = in.readString();
        this.previous = in.readString();
        this.results = in.createTypedArrayList(StarWar.CREATOR);
    }

    public static final Creator<StarWarList> CREATOR = new Creator<StarWarList>() {
        @Override
        public StarWarList createFromParcel(Parcel source) {
            return new StarWarList(source);
        }

        @Override
        public StarWarList[] newArray(int size) {
            return new StarWarList[size];
        }
    };
}
