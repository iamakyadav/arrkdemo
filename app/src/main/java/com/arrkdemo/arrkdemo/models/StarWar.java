package com.arrkdemo.arrkdemo.models;

import android.os.Parcel;
import android.os.Parcelable;

public class StarWar implements Parcelable {

    public String name;
    public String height;
    public String mass;
    public String created;

    public StarWar() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getMass() {
        return mass;
    }

    public void setMass(String mass) {
        this.mass = mass;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.height);
        dest.writeString(this.mass);
        dest.writeString(this.created);
    }

    protected StarWar(Parcel in) {
        this.name = in.readString();
        this.height = in.readString();
        this.mass = in.readString();
        this.created = in.readString();
    }

    public static final Creator<StarWar> CREATOR = new Creator<StarWar>() {
        @Override
        public StarWar createFromParcel(Parcel source) {
            return new StarWar(source);
        }

        @Override
        public StarWar[] newArray(int size) {
            return new StarWar[size];
        }
    };
}
